#!/usr/local/bin/python3
###### INSTALL #######
# sudo pip3 install pyobjc-framework-Quartz
#
###### USAGE #########
# ./surveillance.py
# ./surveillance.py --path /Users/YOUR_USERNAME/cloud/cameramac/

# import the necessary packages
from imutils.video import VideoStream
import argparse
import datetime
import imutils
import time
import cv2
import Quartz

def screenIsLocked():
	return 'CGSSessionScreenIsLocked' in Quartz.CGSessionCopyCurrentDictionary().keys()

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", help="path to the video file. Default webcam")
ap.add_argument("-f", "--fps", type=float, default=0, help="frames per second. Default 0 = as many as possible.")
ap.add_argument("-a", "--min-area", type=int, default=500, help="minimum area size")
ap.add_argument("-d", "--debug", type=int, default=0, help="Debug level, default no debug. 0 = no debug, 1 = don't check for screensaver, 2 = show life video, 3 show Thresh and Delta frames")
ap.add_argument("-p", "--path", type=str, default="/Users/s/cloud/cameramac/", help="Directory to save files with motion detection")
args = vars(ap.parse_args())

path = args["path"]
if path[-1] != '/':
	path += '/'

videoInitialized = False
# loop over the frames of the video
while True:
	# record only if screen is locked and in production (not debugging)
	while not screenIsLocked() and args["debug"] == 0:
		if videoInitialized:
			vs.stop() if args.get("video", None) is None else vs.release()
			vs = None
			videoInitialized = False
		time.sleep(1.0)

	if not videoInitialized:
		# if the video argument is None, then we are reading from webcam
		if args.get("video", None) is None:
			vs = VideoStream(src=0)
			vs.start()
		# otherwise, we are reading from a video file
		else:
			vs = cv2.VideoCapture(args["video"])
		firstFrame = None # initialize the first frame in the video stream
		framesCount = 0
		start_time = time.time() # start time of the loop
		videoInitialized = True


	# grab the current frame and initialize the occupied/unoccupied
	# text
	frame = vs.read()
	frame = frame if args.get("video", None) is None else frame[1]
	text = "Unoccupied"

	# if the frame could not be grabbed, then we have reached the end
	# of the video
	if frame is None:
		break

	# resize the frame, convert it to grayscale, and blur it
	frameSmall = imutils.resize(frame, width=500)
	gray = cv2.cvtColor(frameSmall, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (21, 21), 0)

	# if the first frame is None, initialize it
	if firstFrame is None:
		firstFrame = gray
		continue

	# compute the absolute difference between the current frame and
	# first frame
	frameDelta = cv2.absdiff(firstFrame, gray)
	thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

	# dilate the thresholded image to fill in holes, then find contours
	# on thresholded image
	thresh = cv2.dilate(thresh, None, iterations=2)
	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	cnts = imutils.grab_contours(cnts)

	# loop over the contours
	for c in cnts:
		# if the contour is too small, ignore it
		if cv2.contourArea(c) < args["min_area"]:
			continue

		# if motion detectes, save file
		cv2.imwrite( path + 'cam' + datetime.datetime.now().strftime("%Y%m%d_%H%M%S") + '.jpg', frame)
		# compute the bounding box for the contour, draw it on the frame,
		# and update the text
		if args["debug"] > 1:
			(x, y, w, h) = cv2.boundingRect(c)
			cv2.rectangle(frameSmall, (x, y), (x + w, y + h), (0, 255, 0), 2)
			text = "Occupied"


	# show the frame and record if the user presses a key
	if args["debug"] > 2:
		cv2.imshow("Thresh", thresh)
		cv2.moveWindow("Thresh", 50, 700)
		cv2.imshow("Frame Delta", frameDelta)
		cv2.moveWindow("Frame Delta", 50, 350)

	if args["debug"] > 1:
		# draw the text and timestamp on the frame
		cv2.putText(frameSmall, "Room Status: {}".format(text), (10, 20),
			cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
		cv2.putText(frameSmall, datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S"),
			(10, frameSmall.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

		cv2.imshow("Security Feed", frameSmall)
		cv2.moveWindow("Security Feed", 50, 10)
		key = cv2.waitKey(1) & 0xFF
		# if the `q` key is pressed, break from the lop
		if key == ord("q"):
			break

	framesCount += 1
	if framesCount % 10 == 0:
		firstFrame = gray

	if framesCount == 50:
		framesCount = 0
		print("FPS: ", 1.0 / ((time.time() - start_time) / 50)) # FPS = 1 / time to process loop
		start_time = time.time() # start time of the loop

	if args["fps"] > 0 and args["fps"] < 50: # real max 35 fps
		time.sleep( 1/args["fps"] );

# cleanup the camera and close any open windows
vs.stop() if args.get("video", None) is None else vs.release()
cv2.destroyAllWindows()
